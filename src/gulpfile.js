/**
 * Global Dependencies
 */
var gulp = require('gulp')
var less = require('gulp-less')
var path = require('path')
var cssmin = require('gulp-cssmin')
var uglify = require('gulp-uglify')
var pump = require('pump')
var watch = require('gulp-watch')
var nunjucks = require('gulp-nunjucks-render')
var htmlmin = require('gulp-htmlmin')
var imagemin = require('gulp-imagemin')
var connect = require('gulp-connect')
var del = require('del');
var wiredep = require('wiredep').stream;

/**
 * Configuration
 */
var config = {
  paths: {
    destination: '../dist/',
    js: {
      src: './js/*.js',
      destination: 'assets/js'
    },
    css: {
      src: './less/',
      mainLessFilename: 'main.less',
      destination: 'assets/css'
    },
    html: {
      src: ['./templates/'],
    },
    vendor: {
      src: './vendor/**/*',
      destination: 'assets/vendor/'
    },
    images: {
      src: './images/**/*',
      destination: 'assets/images'
    }
  },
  minifyHtml: false
};

// Clean destination folder
gulp.task('clean', function () {
  del([
    config.paths.destination + '*.html',
    config.paths.destination + config.paths.js.destination,
    config.paths.destination + config.paths.css.destination,
    config.paths.destination + config.paths.images.destination,
  ], { force: true })
});

// Compile CSS
gulp.task('compile:css', function (cb) {
  return gulp.src(config.paths.css.src + config.paths.css.mainLessFilename)
    .pipe(less({
      paths: [path.join(__dirname, 'less', 'includes')]
    }))
    .pipe(cssmin())
    .pipe(gulp.dest(config.paths.destination + config.paths.css.destination))
})

// Compile JavaScript
gulp.task('compile:js', function (cb) {
  pump([
    gulp.src(config.paths.js.src),
    uglify(),
    gulp.dest(config.paths.destination + config.paths.js.destination)
  ], cb)
    .pipe(connect.reload())
})

// Compile HTML
gulp.task('compile:html', function () {
  var proc = gulp.src([config.paths.html.src + '*.html'])
    .pipe(nunjucks({
      path: config.paths.html.src
    }))

  if(config.minifyHtml === true) {
    proc = proc.pipe(htmlmin({collapseWhitespace: true}))
  }

  return proc.pipe(gulp.dest(config.paths.destination))
    .pipe(connect.reload())
})

// Optimize and Copy Images
gulp.task('optimize:images', function () {
  return gulp.src(config.paths.images.src)
    .pipe(imagemin())
    .pipe(gulp.dest(config.paths.destination + config.paths.images.destination))
    .pipe(connect.reload())
})

gulp.task('copy:vendor', function () {
  return gulp.src(config.paths.vendor.src)
    .pipe(gulp.dest(config.paths.destination + config.paths.vendor.destination))
    .pipe(connect.reload())
});

// Watch task
gulp.task('watch', function () {
  connect.server({
    root: config.paths.destination,
    livereload: true
  });

  gulp.watch('./less/**/*.less', ['compile:css'])
  gulp.watch('./js/**/*.js', ['compile:js'])
  gulp.watch('./templates/**/*.html', ['compile:html'])
  gulp.watch('./images/**/*', ['optimize:images'])
  gulp.watch('./vendor/**/*', ['copy:vendor'])
})

gulp.task('default', ['clean', 'compile:js', 'compile:css', 'compile:html', 'optimize:images', 'copy:vendor'])